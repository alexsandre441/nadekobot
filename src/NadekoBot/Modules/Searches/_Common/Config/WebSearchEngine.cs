﻿namespace NadekoBot.Modules.Searches;

public enum WebSearchEngine
{
    Google,
    Searx,
}